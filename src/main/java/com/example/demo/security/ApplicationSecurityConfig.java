package com.example.demo.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

import static com.example.demo.security.ApplicationUserPermission.COURSE_WRITE;
import static com.example.demo.security.ApplicationUserRole.*;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class ApplicationSecurityConfig extends WebSecurityConfigurerAdapter {
    
    // Configuracion del encoder Bcript para los passwords que se vincula con la configuracion PasswordConfig
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public ApplicationSecurityConfig(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }
    
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            // configuracion para deshabilitar el Cross Site Request Forgery
            .csrf().disable()
            // autoriza todas las peticiones
            .authorizeRequests()
            
            // las siguientes rutas seran publicas, excluidas de autentificacion, y seran permitidas todas
            .antMatchers("/", "index", "/css/*", "/js/*").permitAll()
            // con este patron se le asigna al rol estudiante el acceso a el endpoint de la api solamente el estudiante podra verlo y asi se protege o separa el rol del administrador
            .antMatchers("/api/**").hasRole(STUDENT.name())
            // metodo 1: para aplicar los permisos por autoridades en ves de roles en las partes del CRUD
            // la importancia en como se declaran estos antmatchers tiene valor, hay que declararlos de acuerdo aun
            // orden, de hecho despues de http el orden de ejecucion va de arriba hacia abajo, lo primero que pasaria
            // seria el csrf y asi sucesivamente
//            .antMatchers(HttpMethod.DELETE, "/management/api/**").hasAuthority(COURSE_WRITE.getPermission())
//            .antMatchers(HttpMethod.POST, "/management/api/**").hasAuthority(COURSE_WRITE.getPermission())
//            .antMatchers(HttpMethod.PUT, "/management/api/**").hasAuthority(COURSE_WRITE.getPermission())
//            .antMatchers(HttpMethod.GET,"/management/api/**").hasAnyRole(ADMIN.name(), ADMINTRAINEE.name())
            
            // todas las peticiones deben ser autenticadas
            .anyRequest()
            .authenticated()
            .and()
            
            // el mecanismo para autorizar sera httpbasic
//            .httpBasic();
        
            // Autentificacion basado en formulario *desabilitando la autentificacion basica*
            .formLogin()
            .loginPage("/login").permitAll()
            .defaultSuccessUrl("/courses", true);
    }
    
    // sobreescribir el metodo de usuarios, creando uno nuevo con la clase user de spring
    @Override
    @Bean
    protected UserDetailsService userDetailsService() {
        // usuario regular tipo estudiante
        UserDetails juanUser = User.builder()
            .username("juan")
            // se inyecta la dependencia de passwordEncoder con el password en texto plano, esta transforma el texto en BCrypt
            .password(passwordEncoder.encode("password"))
//            .roles(STUDENT.name()) // ROLE_STUDENT
            .authorities(STUDENT.getGrantedAuthorities())
            .build();
    
        // usuario tipo administrador
        UserDetails carlosUser = User.builder()
            .username("carlos")
            .password(passwordEncoder.encode("password123"))
            // aqui es donde se le inyecta el application user role con los permisos que se le dieron en los ENUM, se realizo una importacion estaticacommunity edition vs
//            .roles(ADMIN.name()) // ROLE_ADMIN
            .authorities(ADMIN.getGrantedAuthorities())
            .build();
    
        // usuario tipo administrador
        UserDetails sofiaUser = User.builder()
            .username("sofia")
            .password(passwordEncoder.encode("password123"))
            // aqui es donde se le inyecta el application user role con los permisos que se le dieron en los ENUM, se realizo una importacion estaticacommunity edition vs
//            .roles(ADMINTRAINEE.name()) // ROLE_ADMINTRAINEE
            .authorities(ADMINTRAINEE.getGrantedAuthorities())
            .build();
    
        return new InMemoryUserDetailsManager(
            juanUser,
            carlosUser,
            sofiaUser
        );
    }
}
