package com.example.demo.student;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

// endpoint que responde al rol del administrador, este es el controlador que contiene los metodos CRUD
@RestController
@RequestMapping("management/api/v1/students")
public class StudentManagementController {
    
    // usuarios que contendra la lista
    private static final List<Student> STUDENTS = Arrays.asList(
        new Student(1, "gabriela"),
        new Student(2, "claudia"),
        new Student(3, "anasofia")
    );
    
    // metodo para obtener todos los estudiantes
    @GetMapping
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_ADMINTRAINEE')") // hasRole('ROLE_'), hasAnyRole('ROLE_'), hasAuthority('permission'), hasAnyAuthority
    // ('permission')
    public List<Student> getAllStudents() {
        System.out.println("getAllStudents");
        return STUDENTS;
    }
    
    // metodo para insertar un estudiante a la BD, en el cuerpo de la peticion contendra el objeto estudiante en
    // formato JSON
    @PostMapping
    @PreAuthorize("hasAuthority('student:write')")
    public void registerNewStudent(@RequestBody Student student) {
        System.out.println("registerNewStudent: " + student);
    }
    
    // metodo para eliminar el estudiante, se espera que en la url se reciba el id del estudiante que se quiere
    // eliminar
    @DeleteMapping(path = "{studentId}")
    @PreAuthorize("hasAuthority('student:write')")
    public void deleteStudent(@PathVariable("studentId") Integer studentId) {
        System.out.println("deleteStudent: " + studentId);
    }
    
    // metodo para actualizar un estudiante, se espera en la url el Id del estudiante, en el cuerpo de la peticion va
    // el objeto estudiante
    @PutMapping(path = "{studentId}")
    @PreAuthorize("hasAuthority('student:write')")
    public void updateStudent(@PathVariable("studentId") Integer studentId, @RequestBody Student student) {
        System.out.println("updateStudent" + String.format("%s %s", studentId,  student));
    }
}
