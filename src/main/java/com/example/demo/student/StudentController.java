package com.example.demo.student;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

// Indicamos que es un controlador rest
@RestController
// Indicamos la ruta a la que respondera este controlador
@RequestMapping("api/v1/students")
public class StudentController {
    
    private static final List<Student> STUDENTS = Arrays.asList(
        new Student(1, "Juan Carlos"),
        new Student(2, "Ana Sofia"),
        new Student(3, "Claudia Alexis")
    );
    
    // se mapea el parametro id del estudiante
    @GetMapping(path = "{studentId}")
    public Student getStudent(@PathVariable("studentId") Integer studentId){
        // revisar como utilizar java streams
        return STUDENTS.stream()
                .filter(student -> studentId.equals(student.getStudentId()))
                .findFirst()
                .orElseThrow(() -> new IllegalStateException("Student " + studentId + " does not exists"));
    }
}
